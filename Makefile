.PHONY: deps clean build

deps:
	go get -u ./...

clean: 
	rm -rf ./producer/main
	rm -rf ./consumer/main
	
build:
	GOOS=linux GOARCH=amd64 go build -o producer/main ./producer
	GOOS=linux GOARCH=amd64 go build -o consumer/main ./consumer