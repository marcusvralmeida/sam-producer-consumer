package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

var service *sqs.SQS

func init() {

	sess := session.New(&aws.Config{})
	service = sqs.New(sess)
}
func handler(ctx context.Context, snsEvent events.SNSEvent) {
	queueURL := os.Getenv("QUEUE_URL")
	log.Printf("QUEUE_URL= %s", queueURL)

	for _, record := range snsEvent.Records {
		snsRecord := record.SNS

		fmt.Printf("[%s %s] Message = %s \n", record.EventSource, snsRecord.Timestamp, snsRecord.Message)
		sendParams := &sqs.SendMessageInput{
			MessageBody: aws.String(snsRecord.Message),
			QueueUrl:    aws.String(queueURL),
		}

		sendResp, err := service.SendMessage(sendParams)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(sendResp)
	}
	log.Println("Todas as mensagens foram enviadas!")
}

func main() {
	lambda.Start(handler)
}
